package model

type SprintDateData struct {
	IsActived *int8 `json:"is_actived,omitempty"`
	NextDate *string `json:"next_date,omitempty"`
	TimeInterval *int8 `json:"time_interval,omitempty"`
}
