package model

type SprintDate struct {
	Code *int8 `json:"code,omitempty"`
	Msg *string `json:"msg,omitempty"`
	Data SprintDateData `json:"data,omitempty"`
	First []*string `json:"first,omitempty"`
	SecondTime []struct {
		Level1 []struct {
			Level2 []struct {
				Level3 *string `json:"level3,omitempty"`
			} `json:"level2,omitempty"`
		} `json:"level1,omitempty"`
	} `json:"second_time,omitempty"`
	Third []SprintDateData `json:"third,omitempty"`
	Fourth [][]struct {
		A *string `json:"a,omitempty"`
		AA []*int8 `json:"a_a,omitempty"`
	} `json:"fourth,omitempty"`
}
