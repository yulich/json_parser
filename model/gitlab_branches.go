package model

type GitlabBranches []struct {
	Name *string `json:"name,omitempty"`
	Merged *bool `json:"merged,omitempty"`
	Protected *bool `json:"protected,omitempty"`
	Default *bool `json:"default,omitempty"`
	DevelopersCanPush *bool `json:"developers_can_push,omitempty"`
	DevelopersCanMerge *bool `json:"developers_can_merge,omitempty"`
	CanPush *bool `json:"can_push,omitempty"`
	WebUrl *string `json:"web_url,omitempty"`
	Commit struct {
		AuthorEmail *string `json:"author_email,omitempty"`
		AuthorName *string `json:"author_name,omitempty"`
		AuthoredDate *string `json:"authored_date,omitempty"`
		CommittedDate *string `json:"committed_date,omitempty"`
		CommitterEmail *string `json:"committer_email,omitempty"`
		CommitterName *string `json:"committer_name,omitempty"`
		Id *string `json:"id,omitempty"`
		ShortId *string `json:"short_id,omitempty"`
		Title *string `json:"title,omitempty"`
		Message *string `json:"message,omitempty"`
		ParentIds []*string `json:"parent_ids,omitempty"`
	} `json:"commit,omitempty"`
}
